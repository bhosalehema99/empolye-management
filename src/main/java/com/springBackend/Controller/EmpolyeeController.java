package com.springBackend.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springBackend.exception.ResourceNotFoundException;
import com.springBackend.model.Empolyee;

import com.springBackend.respository.EmpolyeeRespository;

@RestController
@RequestMapping("/api/v1/")
public class EmpolyeeController {

	@Autowired
	private EmpolyeeRespository empolyeeRespository;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/empolyee")
	public List<Empolyee> getAllEmpolyee() {
		return (List<Empolyee>) this.empolyeeRespository.findAll();

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/empolyee")
	public Empolyee createEmpolye(@RequestBody Empolyee empolye) {
		return empolyeeRespository.save(empolye);
	}

	@GetMapping("/empolyee/{id}")
	public ResponseEntity<Empolyee> getEmpolyeById(@PathVariable Long id) {
		Empolyee empolyee = empolyeeRespository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Empolyee Not exist with id " + id));
		return ResponseEntity.ok(empolyee);
	}

	@PutMapping("/empolyee/{id}")
	public ResponseEntity<Empolyee> updateEmpolyee(@PathVariable Long id, @RequestBody Empolyee empolyeeUpdate) {
		Empolyee empolyee = empolyeeRespository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Empolyee Not exist with id " + id));

		empolyee.setFirstName(empolyeeUpdate.getFirstName());
		empolyee.setLastName(empolyeeUpdate.getLastName());
		empolyee.setEmail(empolyeeUpdate.getEmail());

		Empolyee save = empolyeeRespository.save(empolyee);

		return ResponseEntity.ok(save);
	}
	@DeleteMapping("/empolyee/{id}")
	public void deleteEmpolye(@PathVariable Long id) {
		this.empolyeeRespository.deleteById(id);
	}

}
