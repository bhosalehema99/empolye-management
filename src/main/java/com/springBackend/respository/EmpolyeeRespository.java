package com.springBackend.respository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springBackend.model.Empolyee;

public interface EmpolyeeRespository extends CrudRepository<Empolyee, Long>{

	public List<Empolyee> findByfirstName(String Name);
	public void deleteById(Long Id);
}
